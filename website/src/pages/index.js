import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const feature_set_1 = [
  {
    title: <>Mission</>,
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        Our Mission Statement helps us scope our efforts.
      </>
    ),
  },
  {
    title: <>Values</>,
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        Our values help provide a common development philosophy that help us make decisions.
      </>
    ),
  },
  {
    title: <>Code of Conduct</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        We have a Code of Conduct that all community members are expected to uphold.
      </>
    ),
  },
  {
    title: <>Free and Open Source Licensing</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        We license all our code under GPLv3
        and all other content under CC-BY-SA 4.0.
      </>
    ),
  },
];

const feature_set_2 = [
  {
    title: <>User Story Map</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        We have a User Story Map that aggregates featuers needed by one or more clients.
      </>
    ),
  },
  {
    title: <>GitLab</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        We make extensive use of GitLab. 
        We heavily use issues and issue boards to coordinate work.
        We use projects to house code and documents.
      </>
    ),
  },
  {
    title: <>Coordinating Committee</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        We have a Coordinating Committee that meets weekly to coordinate our efforts.
      </>
    ),
  },
];


function Feature4_8({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          <div className={clsx('col col--4', styles.feature)}>
            <div className="text--center">
              <img className={styles.featureImage} src={imgUrl} alt={title} />
            </div>
          </div>
          <div className={clsx('col col--8', styles.feature)}>
            <div className="text--center">
              <h3>{title}</h3>
              <p>{description}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}


function Feature4({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  
  return (
    <div className={clsx('col col--3', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Feature3({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <img className={styles.featureImage} src={useBaseUrl('/img/lfp-logo.png')} alt={'LibreFoodPantry'} />
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {feature_set_1 && feature_set_1.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {feature_set_1.map((props, idx) => (
                  <Feature4 key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        {feature_set_2 && feature_set_2.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {feature_set_2.map((props, idx) => (
                  <Feature3 key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
