FROM alpine:3
RUN apk add --no-cache nodejs yarn npm
WORKDIR /workdir
CMD [ "sh", "-c", "cd website ; yarn run build" ]