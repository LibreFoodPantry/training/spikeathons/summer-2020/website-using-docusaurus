### Docker is required

You'll need Docker installed and running. `docker-compose.yml` is used to manage development.

### Build the builder

Run this anytime builder.dockerfile is modified (by you or when you pull changes).

```bash
docker-compose build builder
```

### Build website

Run after you first clone and anytime the contents of `website/` are modified (by you or when you pull changes).

```bash
docker-compose run --rm builder
```

### Run a webserver

Run this when you want to test the website locally. Once you have started the webserver, you don't need to start it again unless you shut it down. Changes to `website/` will become visible through the webserver as soon as you rebuild the website.

```bash
docker-compose up --detach webserver
```

Open a browserd to http://localhost:8080/

### Stop the local webserver

```bash
docker-compose down
```
